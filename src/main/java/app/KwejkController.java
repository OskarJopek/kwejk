package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import app.repo.ImageRepository;

@Controller
public class KwejkController {

    @Autowired
    private ImageRepository fileRepo;

    @GetMapping("/")
    public String hello(ModelMap modelMap){

        modelMap.addAttribute("attr","I'm atribute from java");
        modelMap.addAttribute("fileNameList", fileRepo.listAllGifs());
        return "hello";

    }

}
