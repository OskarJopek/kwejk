package app;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KwejkApp {

    public static void main(String[] args) {
        SpringApplication.run(KwejkApp.class, args);
    }

}
