package app.repo;

import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Controller(value = "hardcodeRepo")
public class HardcodedRepo implements ImageRepository {

    private List<String> gifsList;

    public HardcodedRepo(){
        gifsList = new LinkedList<>();
        gifsList.add("vincent.gif");
        gifsList.add("andrzej.gif");
    }


    @Override
    public List<String> listAllGifs() {
        return gifsList;
    }

}
