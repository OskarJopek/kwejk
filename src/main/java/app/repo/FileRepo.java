package app.repo;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component(value = "fileRepo")
public class FileRepo implements ImageRepository {

    private List<String> fileList;

    public FileRepo(){

        URL resource = getClass().getResource("/static");

        try {
            Path pathToString = Paths.get(resource.toURI());
            Path imagesPath = pathToString.resolve("images");

            fileList = Files.list(imagesPath)
                    .filter(Files::isRegularFile)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(toList());

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> listAllGifs() {

        return fileList;
    }
}
